# -------------------- IMPORTS ONLY -----------------------

# libs for the sql stuff:
import pyodbc  # acces to database
# libs for the http server:
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer  # deals with http server
# libs for security:
from hashlib import sha256
from random import randrange

# our libs:
from _lib.http import *  # for parsing

# other libs
import os  # in order to handle files in any os
import shutil  # allows you to safely copy files
import socket
import threading  # allows threading (a few tasks at once)
import time
import urllib  # deals with parsing

# -------------------- FINALS ONLY ----------------------


PORT_NUMBER = 80
PATH_TO_PROJECT = os.getcwd()[:-7]  # gets current folder and takes only the path to the project
DATABASE_NAME = 'MainDB'  # name of one of our databases
DATABASE_PATH = PATH_TO_PROJECT + r'\databases/'
DATABASE_FILE = DATABASE_PATH + DATABASE_NAME + ".accdb"
PAGES_DIRECTORY = PATH_TO_PROJECT + r"\pages"  # r causes it to ignore the backslash
ROOT_DIRECTORY = PATH_TO_PROJECT + r"\users" + "/"  # path to the folder where the users save there files

# -------------------- PREMAIN ONLY ---------------------

#  connecting to the database:
connectionString = "Driver={Microsoft Access Driver (*.mdb, *.accdb)};Dbq=%s" % DATABASE_FILE
DBCONNECTION = pyodbc.connect(connectionString)

# opens the html pages and puts them into a string:
P_login = open(PAGES_DIRECTORY + r"\login.html").read()
P_login_error = open(PAGES_DIRECTORY + r"\login_error.html").read()

P_after_login = open(PAGES_DIRECTORY + r"\after_login.html").read()
P_logout = open(PAGES_DIRECTORY + r"\logout.html").read()

P_sign = open(PAGES_DIRECTORY + r"\sign.html").read()
P_sign_error = open(PAGES_DIRECTORY + r"\sign_error.html").read()

P_personal_dirs = open(PAGES_DIRECTORY + r"\personal_dirs.html").read()
P_personal_sign = open(PAGES_DIRECTORY + r"\personal_sign.html").read()

P_404_Error = open(PAGES_DIRECTORY + r"\error.html").read()
P_personal_Error = open(PAGES_DIRECTORY + r"\personal_error.html").read()

LOGO = open(PAGES_DIRECTORY + r"\cloud.png", "rb").read()


# -------------------- FUNCTIONS ONLY --------------------

def get_size(start_path='.'):
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            total_size += os.path.getsize(fp)
    return total_size


def check_password(username, password):
    return hashed(password) == get_record_by_username(username).password


def username_exist(username):
    return get_record_by_username(username) is not None


def get_record_by_username(username):
    return get_record_by_field("username", username)


def record_exist(field_name, field_value):
    return get_record_by_field(field_name, field_value) is not None


def get_record_by_field(field_name, field_value):
    """

    :param field_name: the field in the DB that should recognize the record(like an unique name or something)
    :param field_value: the value of that field
    :return: the record which in it field_name = field_value, like username = "dovi". or None if there is no such record
    """
    cursor = DBCONNECTION.execute("SELECT * FROM users WHERE %s = ?" % field_name, field_value)
    records = cursor.fetchall()
    try:
        return records[0]  # if there are some records, bring only the first one
    except:
        return None


def update_record_by_field(field_to_identify, value_to_identify, field_to_change, value_to_change):
    if record_exist(field_to_identify, value_to_identify):
        DBCONNECTION.execute("UPDATE users SET %s = ? WHERE %s = ?" % (field_to_change, field_to_identify),
                             value_to_change, value_to_identify)
        DBCONNECTION.commit()
        return
    raise Exception("record doesn't exist")


def update_cookie(name, cookie):
    update_record_by_field("username", name, "cookie", cookie)


def insert_new_to_table(username, password, nickname, *args):
    DBCONNECTION.execute("INSERT INTO users VALUES (?,?,?,?,?)",
                         (nickname, username, hashed(password), new_cookie(args), 0))
    DBCONNECTION.commit()


def new_cookie(args):
    return hashed(str(randrange(10000000)))


def hashed(password):
    return sha256(password).hexdigest()


# -------------------- CLASSES ONLY ---------------------

class Id(object):
    def __init__(self, code=0, name=None):
        self.code = code  # 0 if guest, 1 if member
        self.name = name  # None if guest(guest doesn't have name)


def make_personal_error(name, message):
    """

    :param name: username
    :param message: the error message to show the user
    :return: html page with an error page with the message and name
    """
    return P_personal_Error % (name, name, message)


def make_personal_dirs_page(username, path=None, message=""):
    """
    :param path: path for the directory to display
    :return: html page. with directories and files display for the given path
    """
    if not path:  # same as path is None
        path = [username]
    space = ""
    space_used_bytes = int(get_record_by_username(username).spaceUsed)
    if space_used_bytes > 1000:
        space_used_kilobytes = space_used_bytes / 1000.0
        if space_used_kilobytes > 1000:
            space = str(space_used_kilobytes / 1000) + " MB"
        else:
            space = str(space_used_kilobytes) + " KB"
    else:
        space = str(space_used_bytes) + " B"
    path_display = []
    dirs_display = []
    files_display = []
    curpath = ""  # current path
    for d in path:
        curpath += d + "/"  # d is the name of the current directory while curpath is the path to the current directory
        path_display.append(
            '<a href="/%s">%s</a>' % (curpath, d))  # this is how it will appear in the HTML page
    diroot = ROOT_DIRECTORY + curpath  # the current path to the folder
    root = os.listdir(diroot)  # returns a list of all the things in the current folder
    for d in root:
        if os.path.isdir(diroot + d):  # checks if it's a folder
            dirs_display.append(
                '<input class = "hidden" type = "checkbox" name = "del_%s"><a href="/%s">%s</a>' % (
                    d, curpath + d, d))  # this is how it will appear in the HTML page
        else:
            files_display.append(
                '<input class = "hidden" type = "checkbox" name = "del_%s"><a href="/%s" download = "%s">%s</a>' % (
                    d, curpath + d, d,
                    d))  # this is how it will appear in the HTML page + download that tells the browser to download it
    path_display = "> ".join(path_display)
    dirs_display = "<br>".join(dirs_display)  # break line between folders
    files_display = "<br>".join(files_display)  # break line between files
    if message:
        message = "<a><b>{}</b></a><br>".format(message)
    return P_personal_dirs % (username, message, space, path_display, dirs_display, files_display)


def save_file(rfile, username, request_path, boundary, filename, content_length):
    with open(request_path + filename, "wb+") as new_file:
        line1 = ""
        while True:
            line2 = line1
            line1 = rfile.readline()
            if boundary in line1:
                new_file.write(line2[:-2])
                break
            if line2:
                new_file.write(line2)
    space_used = int(get_record_by_username(username).spaceUsed)
    space_used += os.path.getsize(request_path + filename) - content_length + 20
    update_record_by_field("username", username, "spaceUsed", str(space_used))
    rfile.close()


def create_dir(path, name):
    """

    :param path: path to the directory where the new dir should be created in
    :param name: name of the new directory
    :return: None
    """
    os.mkdir(path + name)  # makes a new folder in the correct path and with the right name


class MyHandler(BaseHTTPRequestHandler):
    # This class handles any incoming request (POST and GET) from
    # the browser
    def finish(self):
        if not self.wfile.closed:
            try:
                self.wfile.flush()
            except socket.error:
                # A final socket error may have occurred here, such as
                # the local error ECONNABORTED.
                pass
        self.wfile.close()
        try:
            if self.not_done_with_rfile:
                pass
        except:
            self.rfile.close()

    def send_file(self, path):
        """

        :param path: path to the file to send
        :return: None. wfile now contains the file
        """
        with open(path, 'rb') as content:  # with & as -> content = open(path, "rb")
            shutil.copyfileobj(content, self.wfile)  # copies it safely to w.file which is the file sent to the user

    def identify(self):
        identity = Id()
        if "cookie" in self.headers:
            cookies_obj = parse_cookies(self.headers["cookie"])
            if "key" in cookies_obj.cookies:
                cookie = cookies_obj.key  # that's how the cookie that we want is called(we called it that way, in do_post)
                if not cookie == '':
                    if record_exist("cookie", cookie):  # if there's one with that cookie
                        identity.code = 1
                        identity.name = get_record_by_field("cookie", cookie).username
        return identity

    def sending_defaults(self):
        """

        :return: wfile now contains the data it needs to allow sending the page or the file
        """
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        return

    def do_GET(self):
        """
        handles GET requests from the browser.

        :return: None
        """
        if self.path == '/favicon.ico':  # favicon = favorite icon = the icon which the browser put in the favorite list
            self.send_response(200)
            self.send_header('Content-type', 'image/png')
            self.end_headers()
            self.wfile.write(LOGO)  # a nice picture of a cloud
            return
        # just data for the server keeper:
        print "\nGET request:" \
              "\n\treceived from: {}" \
              "\n\tat: {}\n" \
            .format(':'.join([unicode(i) for i in self.client_address]),
                    time.ctime(time.time()))
        page = None  # for the moment. simply to declare page/
        path = urllib.unquote(self.path).decode('utf8')  # decode to allow hebrew names for files
        obj = parse_parameters(path)
        path = obj.path
        identity = self.identify()
        if identity.code == 1:  # if the user is a member. member is identified with 1
            username = identity.name
            if path == []:  # if empty we give his main folder
                page = make_personal_dirs_page(username)
            elif path[0] == "logout":
                update_cookie(username, "")
                page = P_logout
            elif path[0] == "sign":
                page = P_personal_sign % username
            else:
                if path[
                    0] == username:  # if user asks for access to a page that he can have access to we give it to him
                    requested_path = ROOT_DIRECTORY + "/".join(path)
                    if os.path.isdir(requested_path):
                        page = make_personal_dirs_page(username, path)
                    else:
                        if os.path.isfile(requested_path):
                            self.send_response(200)
                            self.send_header('Content-type', 'stam')
                            self.end_headers()
                            self.send_file(requested_path)
                            return
                        else:
                            page = make_personal_error(username,
                                                       "The path you've requested does not exist please request a valid file path")
                elif username_exist(path[0]):
                    page = make_personal_error(username,
                                               "you have no permission to access this path (%s)" % "/".join(path))
                else:
                    page = make_personal_error(username, "this is not a legal path (%s)" % "/".join(path))

        else:  # meaning you are a guest
            if not path:  # if there is no path we will display the login page
                page = P_login
            elif path[0] == "sign":
                page = P_sign
            else:
                page = P_login_error % "you can't enter as a guest, PLEASE LOGIN!"
        self.sending_defaults()
        self.wfile.write(page.encode("utf-8"))  # encode - to allow hebrew pages
        return

    def do_POST(self):
        """
        handles POST requests from the browser.

        :return: None
        """
        identity = self.identify()
        content_length = int(self.headers['Content-Length'])
        if "content-type" in self.headers:
            content_type = self.headers['content-type']
        else:
            content_type = "application/x-www-form-urlencoded"  # application is great, parameters are like the url in GET just in POST. In short, parsing made easy.
        if not "multipart/form-data" in content_type:  # making sure it's easy to parse/ If it's multipart read may cause many problems
            post_data = self.rfile.read(
                content_length)  # reads a number of chars in rfile (the file with what the user sent)
            args = parse_post_data(post_data, content_type)  # parses the POST data
        else:  # it is a multipart and we will not do read
            args = stam()
            args.event = "file"
            args.attrs = {}
        # just data for the server keeper:
        print "\nPOST request:" \
              "\n\treceived from: {}" \
              "\n\tat: {}\n".format(':'.join([str(i) for i in self.client_address]),
                                    time.ctime(time.time()), )

        path = urllib.unquote(self.path).decode('utf8')  # to allow hebrew
        obj = parse_parameters(path)
        path = obj.path
        if identity.code == 0:  # user is a guest
            if not path:
                self.login(args)
            elif path[0] == "sign":
                self.sign(args)
        else:  # the user is a member (GOOD FOR HIM!) we will add a new file/directory/folder (without folding her because that's not polite)
            page = None
            username = identity.name
            request_path = ""
            flag = False  # used as a flag (does not represent a country or idealogy)
            if len(path) == 0:  # if there is no path we add the users name to know which directory to use
                request_path = ROOT_DIRECTORY + username + "/"
                flag = True
            elif path[0] == username:
                request_path = ROOT_DIRECTORY + "/".join(path) + "/"
                flag = True
            if flag:  # asking for the path that he has access to
                if os.path.isdir(request_path):  # if it's a folder...
                    if args.event == "dir":
                        filename = args.new_directory.decode("utf-8")  # for hebrew
                        path_new_dir = request_path
                        if not os.path.isdir(path_new_dir + filename):  # if there is no other folder/directory with the same name
                            create_dir(path_new_dir, filename)  # creates a new directory
                            page = make_personal_dirs_page(username, path)  # shows the HTML page
                        else:
                            page = make_personal_error(username,
                                                       "Directory name already exists, try a different name")
                    elif args.event == "file":  # he wants to add a file
                        #  hours and hours of hard work of parsing:(
                        type_data = content_type.split("; ")
                        boundary = type_data[1].split("=")[1]
                        index = 0
                        filename = ""
                        while True:
                            line = self.rfile.readline()
                            if index == 1:
                                usful_data = line.split("; ")
                                filename = usful_data[2].split("=")[1][1:-3].decode("utf-8")
                            elif index == 2:
                                filetype = line.split(": ")[1]
                            elif index == 3:
                                break
                            index += 1
                        if not os.path.isfile(request_path + filename):
                            space_used = int(get_record_by_username(username).spaceUsed)
                            if space_used + content_length - 20 > 5000000:
                                page = make_personal_error(username,
                                                           "you have used all your space(5 MB) please delete files.")
                            else:
                                self.not_done_with_rfile = True
                                update_record_by_field("username", username, "spaceUsed",
                                                       str(space_used + content_length - 20))
                                t = threading.Thread(name="save_file", target=save_file,
                                                     args=(self.rfile, username, request_path, boundary,
                                                           filename, content_length))
                                t.setDaemon(True)
                                t.start()
                                page = make_personal_dirs_page(username, path)
                        else:
                            page = make_personal_error(username, "filename(%s) already exist" % filename)
                    elif args.event == "del":
                        space_used = int(get_record_by_username(username).spaceUsed)
                        for filename in args.__dict__:
                            if filename.startswith("del_"):
                                filename = filename.decode("utf-8")
                                filename = filename[4:]
                                filepath = request_path + filename
                                if os.path.isfile(filepath):
                                    space_used -= os.path.getsize(filepath)
                                    os.remove(filepath)
                                elif os.path.isdir(filepath):
                                    space_used -= get_size(filepath)
                                    shutil.rmtree(filepath)  # erases the entire tree of the folder
                        if space_used < 0:
                            space_used = 0
                        update_record_by_field("username", username.decode("utf-8"), "spaceUsed", str(space_used))
                        page = make_personal_dirs_page(username, path)
                    else:
                        page = make_personal_error(username, "sorry, your request was illegal")
                else:
                    page = make_personal_error(username, "path doesn't exist: %s" % "/".join(path))
            else:
                if username_exist(path[0]):
                    page = make_personal_error(username,
                                               "you have no permission to this path(%s)" % "/".join(path))
                else:
                    page = make_personal_error(username, "this is not a legal path(%s)" % "/".join(path))
            self.sending_defaults()
            self.wfile.write(page.encode("utf-8"))
        return

    def sign(self, data_object):
        """
        handle sign parameters to create new user
    
        :param data_object: object with all the data needed
        :return: None
        """
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        args = data_object
        username = args.username.strip()
        page = None
        if username_exist(username):
            page = P_sign_error % "The username you picked is already taken, please try another"
        else:
            insert_new_to_table(username, args.password, "True")  # saving in the db
            create_dir(ROOT_DIRECTORY, username)  # creating a new file for the user that just signed up
            cookie = new_cookie(args)  # creates a new cookie (make sure cookie monster doesn't find it)
            update_cookie(username, cookie)  # updates cookie data in db
            self.send_header('Set-Cookie', "key=%s" % cookie)  # HTTP syntax
            page = P_after_login  # give the user the after login page with stuff like data received successfully and what not!
        self.end_headers()
        self.wfile.write(page.encode("utf-8"))

    def login(self, data_object):
        """
        handle sign parameters to create cookie for session and sent it to the user

        :param data_object: object with all the data needed
        :return: None
        """
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        args = data_object
        username = args.username.strip()  # strip removes spaces from the sides of the string
        page = None
        if not username_exist(username):
            page = P_login_error % "Username doesn't exist, please try again"
        elif not check_password(username.strip(), args.password.strip()):
            page = P_login_error % "Password is incorrect, please try again"
        else:  # if username and password are correct we will create a cookie and give the user his page
            # self.send_response(200)
            # self.send_header('Content-type', 'text/html')
            cookie = new_cookie(args)  # creates a new cookie
            update_cookie(username, cookie)  # updates cookie
            self.send_header('Set-Cookie', "key=%s" % cookie)  # in HTTP syntax :)
            page = make_personal_dirs_page(username)  # the user is given his page
        self.end_headers()
        self.wfile.write(page.encode("utf-8"))  # for the HEBREW


class MyServer(HTTPServer):
    def _handle_request_noblock(self):
        """Handle one request, without blocking
        (we have overwritten the "_handle_request_noblock" method because we wanted to start a new thread
         for each request so that one request wouldn't block the others).

        I assume that select.select has returned that the socket is
        readable before this function was called, so there should be
        no risk of blocking in get_request().
        """
        try:
            request, client_address = self.get_request()
        except socket.error:
            return
        if self.verify_request(request, client_address):
            try:
                # creates a new thread in daemon mode and starts it in order to deal with a number of requests at the
                # same time to be able to use do_post and do_get
                t = threading.Thread(name="one_request", target=self.process_request, args=(request, client_address))
                t.setDaemon(True)  # a mode which allows it to run in the background
                t.start()
            except:
                self.handle_error(request, client_address)
                self.shutdown_request(request)
        else:
            self.shutdown_request(request)


# -------------------- MAIN ------------------------------

def main():
    # Creates a web server and defines the handler to manage the incoming requests:
    server = MyServer(('', PORT_NUMBER), MyHandler)
    print 'Started httpserver on port ', PORT_NUMBER
    server.serve_forever()  # Wait forever for incoming http requests


if __name__ == '__main__':
    main()
