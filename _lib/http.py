import urllib


def parse_parameters(url_end):
    """
    parses the url

    :param url_end: the end of the URL. if the URL is "www.google.com/sign/?q=123&t=101" so url_end should be "/sign/?q=123&t=101"
    :return: an object which contains the data that can be understood from the url_end: parameters, path
    """
    obj = url_object()  # just an empty object that can use setattr and getattr
    path = None  # for the moment.
    pars = None  # for the moment.
    for i in range(len(url_end)):  # checks when the parameters part in the url starts
        if url_end[i] == '?': # ? indicates that the parameters start
            path = url_end[:i]
            pars = url_end[i + 1:]
            break
    if not path:  # if there isn't '?' in the url_end
        path = url_end
        pars = None
    obj.rawpath = path
    obj.rawpars = pars

    dirs = path.split("/") # array of the path parts of the url
    path = []
    for i in range(len(dirs)):
        if dirs[i] != '':
            path.append(dirs[i])
    obj.path = path

    obj.file = None
    if dirs[-1] != '':
        obj.file = dirs[-1]

    if pars:
        parspairs = pars.split("&")
        obj.parpairs = parspairs

        for pair in parspairs:
            pair = pair.split('=')
            setattr(obj, pair[0], pair[1])
    return obj


def parse_post_data(data, content_type):
    """

    :param data: raw data from post request
    :param content_type: type of the content
    :return: obj that contains the parsed data with attributes
    """
    obj = postdata_object()
    obj.event = 0
    if content_type == "application/x-www-form-urlencoded":  # from html form requests
        data = urllib.unquote_plus(data).decode('utf-8')
        parspairs = data.split("&")
        obj.attrs = {}
        for pair in parspairs:
            pair = pair.encode("utf-8").split('=')
            setattr(obj, pair[0], pair[1])
            obj.attrs[pair[0]] = pair[1]
            if pair[0] == "new_directory":
                obj.event = "dir"
    return obj


def parse_cookies(raw_header):
    """
    parser for cookies

    :param raw_header: the cookie header in http
    :return: obj that contains all the cookies in the raw_header
    """
    obj = cookie_object()  # just an empty object that can use setattr and getattr
    cookie_pairs = raw_header.split(";")
    obj.cookies = {}
    for cookie_pair in cookie_pairs:
        cookie_tup = cookie_pair.split("=")
        setattr(obj, *cookie_tup)
        obj.cookies[cookie_tup[0]] = cookie_tup[1]
    return obj


class stam:
    def __str__(self):
        string = ''

        for i in self.__dict__:
            string += str(i) + ' : ' + str(self.__dict__[i]) + '\n'

        return string


class cookie_object(stam):
    pass


class postdata_object(stam):
    pass


class url_object(stam):
    pass
