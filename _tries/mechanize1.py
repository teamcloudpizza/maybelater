from ftplib import FTP
ftp = FTP('ftp.debian.org')     # connect to host, default port
ftp.login()                     # user anonymous, passwd anonymous@
#'230 Login successful.'
ftp.cwd('debian')               # change into "debian" directory
ftp.retrlines('LIST')           # list directory contents
#'226 Directory send OK.'
ftp.retrbinary('RETR README', open('README', 'wb').write)
#'226 Transfer complete.'
ftp.quit()