import os
import pyodbc

dataFile = "Northwind.mdb"
databaseFile = os.getcwd() + "\\" + dataFile
connectionString = "Driver={Microsoft Access Driver (*.mdb, *.accdb)};Dbq=%s" % databaseFile
dbConnection = pyodbc.connect(connectionString)
cursor = dbConnection.cursor()
cursor.execute("select top 5 * from Suppliers")
rows = cursor.fetchall()
for row in rows:
    print row.CompanyName