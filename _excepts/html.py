def replace(line,*args):
    places = line.split("%%")
    for i in range(1,len(args)*2+1,2):
        print str(args[(i-1)/2])
        places[i] = str(args[(i-1)/2])
    return "".join(places)

def linebytag(lines,tag):
    for i in range(len(lines)):
        if tag in lines[i]:
            return i
    raise Exception("no such tag in file")