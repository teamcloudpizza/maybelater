from random import randrange

letters = "aeouirstnpbcdlmfgjkhqvwxyz"
nikuds = letters[:5]
common_letters = letters[5:17]
common_letters2 = letters[:17]
others = letters[17:]

def random_letter(options = None,nikud = False, common = False):
    if not options:
        global letters
        options = letters
    if nikud:
        global nikuds
        options = nikuds
    if common:
        global common_letters2
        options = common_letters2
    return options[randrange(options.__len__())]

def random_word(len = 5, smart = False):
    if not smart:
        return "".join([random_letter() for i in range(len)])

    global letters
    global common_letters
    global nikuds
    word = ""
    for i in range(len):
        if i%4 == 0:
            options = letters
        elif i%2 == 0:
            options = common_letters
        else:
            options = nikuds
        word += random_letter(options)
    return word


if __name__ == '__main__':
    print random_word(20,3)
