# goal - 'hello world' from the server in python to the browser
# the browser browse http://127.0.0.1:80/ and the pythonic server reply a html page that will be shown by the browser


# !/usr/bin/python
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer

from _excepts.html import *
# our libs:
from _lib.http import *

PORT_NUMBER = 80

page2 = open(r"C:\Users\user\Desktop\ariel\falafel\pages\exm2.html")
page1 = open(r"C:\Users\user\Desktop\ariel\falafel\pages\exm1.html")
page1t = page1.read()
page2t = page2.read()
# This class will handles any incoming request from
# the browser
class myHandler(BaseHTTPRequestHandler):
    # Handler for the GET requests
    def do_GET(self):
        page1lines = page1t.split("\n")
        obj =  parse_parameters(self.path)
        path = obj.rawpath
        file = obj.file
        line2 = linebytag(page1lines,"%%file%%")
        if file:
            page1lines[line2] = replace(page1lines[line2], file)
        else:
            del page1lines[line2]
        line1 = linebytag(page1lines,"%%path%%")
        page1lines[line1] = replace(page1lines[line1],path)

        link = "http://127.0.0.1"
        dirs = obj.path
        dirs[0] = ''
        if obj.file:
            dirs[-1] = ''
        else:
            if len(dirs) > 2:
                del dirs[-2]
        link+= "/".join(dirs) + d(obj.rawpars)

        line = linebytag(page1lines,"%%link%%")
        page1lines[line] = replace(page1lines[line],link)

        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

        pagetosend = "\r".join(page1lines)
        self.wfile.write(pagetosend)
        return

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        post_data = self.rfile.read(content_length)
        print parse_post_data(post_data)
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write(page2t)
        return

def d(t):
    if t:
        return "?" + t
    return ''

try:
    # Create a web server and define the handler to manage the
    # incoming request
    server = HTTPServer(('', PORT_NUMBER), myHandler)
    print 'Started httpserver on port ', PORT_NUMBER

    # Wait forever for incoming htto requests
    server.serve_forever()

except KeyboardInterrupt:
    print '^C received, shutting down the web server'
    server.socket.close()

    # import time
# import BaseHTTPServer
#
#
# HOST_NAME = 'falafelcloud.com' # !!!REMEMBER TO CHANGE THIS!!!
# PORT_NUMBER = 80 # Maybe set this to 9000.
#
#
# class MyHandler(BaseHTTPServer.BaseHTTPRequestHandler):
#     def do_HEAD(s):
#         s.send_response(200)
#         s.send_header("Content-type", "text/html")
#         s.end_headers()
#     def do_GET(s):
#         """Respond to a GET request."""
#         s.send_response(200)
#         s.send_header("Content-type", "text/html")
#         s.end_headers()
#         s.wfile.write("<html><head><title>Title goes here.</title></head>")
#         s.wfile.write("<body><p>This is a test.</p>")
#         # If someone went to "http://something.somewhere.net/foo/bar/",
#         # then s.path equals "/foo/bar/".
#         s.wfile.write("<p>You accessed path: %s</p>" % s.path)
#         s.wfile.write("</body></html>")
#
# if __name__ == '__main__':
#     server_class = BaseHTTPServer.HTTPServer
#     httpd = server_class((HOST_NAME, PORT_NUMBER), MyHandler)
#     print time.asctime(), "Server Starts - %s:%s" % (HOST_NAME, PORT_NUMBER)
#     try:
#         httpd.serve_forever()
#     except KeyboardInterrupt:
#         pass
#     httpd.server_close()
#     print time.asctime(), "Server Stops - %s:%s" % (HOST_NAME, PORT_NUMBER)