types = {0:"AutoNumber",1:"Text",2:"Integer",3:"Date/Time",4:"Yes/No",5:"Double",6:"Long"}

class SqlHandler(object):
    def __init__(self,connection,*tablesTuples):
        self.connect = connection
        self.cursors = []
        self.current_cursor = self.new_cursor()
        self.tables = {}
        self.meet_tables(*tablesTuples)
        return

    def __getitem__(self, item):
        return self.tables.__getitem__(item)

    def execute(self,command,*param):
        print command,'\n'
        cursor = self.connect.execute(command,*param)
        self.connect.commit()
        return cursor

    def execursor(self,command,*param):
        print command,'\n'
        self.current_cursor = self.new_cursor()
        cursor = self.current_cursor.execute(command,*param)
        return cursor

    def new_cursor(self):
        cursor = self.connect.cursor()
        self.cursors.append(cursor)
        return cursor

    def add_table(self,name,colTups):
        sql_command = "CREATE TABLE %s \n("%(name,)
        for i in range(len(colTups)):
            if i != 0:
                sql_command+=',\n'
            sql_command+= "%s %s"%(colTups[i][0],types[colTups[i][1]])
        sql_command+=')'
        self.execute(sql_command)
        self.meet_table(name,colTups)

        return
    def meet_table(self,name,colTups):
        self.tables[name] = SqlTable(self, name, colTups)

    def meet_tables(self,*listOfTables):
        for tup in listOfTables:
            self.meet_table(*tup)


class SqlTable(object):
    def __init__(self,handler,name,colTups):
        self.name = name
        self.handler = handler
        self.colTups = colTups

    def cols_names(self):
        return [tup[0] for tup in self.colTups]

    def add_record(self,values):
        sql_command = "INSERT INTO %s\nVALUES (" % (self.name,)
        sql_command += ','.join(['?' for i in values])
        sql_command += ')'
        self.handler.execute(sql_command,values)
        return

    # def convert(self,value,type1):
    #     if type1 == 1:
    #         return '"' + value + '"'
    #     return value
    def drop(self):
        self.handler.execute("DROP TABLE ?",self.name)

    def get_record(self,cols = None,condition = None):
        if not cols:
            cols = self.cols_names()
        sql_command = 'SELECT '
        sql_command += ', '.join(["?" for i in cols])
        sql_command += ' FROM ?'
        if condition:
            sql_command += ' WHERE %s'%condition
        print list(cols)+[self.name]
        ezer(*(list(cols)+[self.name]))
        cursor = self.handler.execursor(sql_command,*['nummm', 'mes', 'number1', 'kimat_last'])
        return cursor.fetchall()


